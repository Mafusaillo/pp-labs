import java.util.stream.IntStream;

public class FibonacciNum {
    int[] fibonacciNumbers;
    int index;
    int value;

    FibonacciNum(int numberOfFirst) {
        fibonacciNumbers = new int[numberOfFirst];
        int firstNum = 0;
        int secondNum = 1;
        int fibNext;
        for (int i = 0; i < numberOfFirst; i++) {
            fibonacciNumbers[i] = firstNum;
            index = i;
            value = firstNum;
            fibNext = firstNum + secondNum;
            firstNum = secondNum;
            secondNum = fibNext;
        }
    }
    public static int fibonacciSum(FibonacciNum obj) {
        return IntStream.of(obj.fibonacciNumbers).sum();
    }
}

