package main;

import static customer.CustomerUtils.input;
import static customer.CustomerUtils.methodUser;

public class Main {
    public static void main(String[] args) {
        methodUser(input());
    }
}

/*

Entered data

1
Prots
Danylo
Olegovych
Lviv city
255
789,56
Yes
2
Bolinovskyy
Andrew
Andriyovych
Zhovkva city
365
-245,69
Yes
3
Vantuh
Matthew
Romanovych
Odessa city
789
23,5
Yes
4
Gnatkevych
Yulia
Dmytrivna
London city
693
-56,3
No
 */
