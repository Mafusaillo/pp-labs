package customer;

public class Customer {

    private final int id;
    private final String lastName;
    private final String firstName;
    private final String surname;
    private String address;
    private long cardNumber;

    public int getId() {
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String getAddress() {
        return address;
    }

    private double balance;

    public Customer(int id, String lastName, String firstName, String surname, String address, long cardNumber, double balance) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.surname = surname;
        this.address = address;
        this.cardNumber = cardNumber;
        this.balance = balance;
    }
    @Override
    public String toString() {
        return id + " | " + lastName + " | " + firstName + " | " + surname + " | " + address + " | " + cardNumber + " | " + balance;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public double getBalance() {
        return balance;
    }



    public String setAddress(String address) {
        return this.address = address;
    }

    public long setCardNumber(long cardNumber) {
        return this.cardNumber = cardNumber;
    }

    public double setBalance(double balance) {
        return this.balance = balance;
    }
}