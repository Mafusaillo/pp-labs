package customer;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class CustomerUtils {


    public static void findSameName(Customer[] customers, String name) {
        System.out.println("The Customers with name " + name + " are : ");
        for (Customer customer : customers) {
            if (customer.getFirstName().equals(name)) {
                System.out.println(customer);
            }
        }
        System.out.println();
    }

    public static void findCardDiapason(Customer[] customers, String diapason) {
        System.out.println("Person with card number in range of " + diapason + " are : ");
        for (Customer customer : customers) {
            String[] diapasonNums = diapason.split("-");
            int from = Integer.parseInt(diapasonNums[0]);
            int to = Integer.parseInt(diapasonNums[1]);
            if (customer.getCardNumber() <= to && customer.getCardNumber() >= from) {
                System.out.println(customer);
            }
        }
        System.out.println();
    }

    public static void findDebtor(Customer[] customers) {
        System.out.println("Debtors are : ");
        for (Customer customer : customers) {
            if (customer.getBalance() < 0) {
                System.out.println(customer);
            }
        }
        System.out.println();
    }

    public static Customer[] input() {
        List<Customer> customers = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        String checker = "Yes";
        while (checker.equalsIgnoreCase("yes")) {
            System.out.println("Enter id >>");
            int id = Integer.parseInt(scanner.next());
            System.out.println("Enter last name >>");
            String lastName = scanner.next();
            System.out.println("Enter first name >>");
            String firstName = scanner.next();
            System.out.println("Enter surname >>");
            String surname = scanner.next();
            System.out.println("Enter address >>");
            String address = scanner.next();
            System.out.println("Enter card number >>");
            long cardNumber = Long.parseLong(scanner.next());
            System.out.println("Enter balance >>");
            double balance = Double.parseDouble(scanner.next());
            System.out.println("Do you wish to continue ? (Yes/No)");
            checker = scanner.next();
            if (checker.equalsIgnoreCase("yes") || checker.equalsIgnoreCase("no")) {
                customers.add(new Customer(id, lastName, firstName, surname, address, cardNumber, balance));
            } else {
                throw new InputMismatchException();
            }
        }
        return customers.toArray(new Customer[0]);
    }

    public static void methodUser (Customer[] customers){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter name of persons you would like to find >> ");
        String name = scanner.next();
        CustomerUtils.findSameName(customers, name);
        System.out.println("Enter diapason of cards you want to find >> (in form xxx-xxx)");
        String diapason = scanner.next();
        CustomerUtils.findCardDiapason(customers, diapason);
        CustomerUtils.findDebtor(customers);
    }

}
