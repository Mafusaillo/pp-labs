import java.util.Scanner;

class FibonacciCall {
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter N to check the sum of N Fibonacci num >> ");
        int userInput = input.nextInt();
        if (userInput < 0){
            throw new Exception("Incorrect Number!");
        }
        FibonacciNum obj = new FibonacciNum(userInput);
        System.out.println("The sum of " + userInput + " is >> " + FibonacciNum.fibonacciSum(obj));
    }
}